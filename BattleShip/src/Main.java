
import battleship.Game;
import battleship.Graphic.GUIImpl;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Barbara
 */
public class Main {
    public static void main(String[] args) {

        GUIImpl gui = new GUIImpl();
        Game g = new Game(gui);
        
        gui.disableP2();
        gui.enableP1();
        g.p2PlaceRandom();

        
    }
}
