
package battleship;
import java.util.Arrays;

public class Helper {
    public static Coordinate convertToXY(String s) { //C8
        char[] letters = "ABCDEFGHIL".toCharArray();

        int x = Arrays.asList(letters).indexOf(s.charAt(0));

        int y = Integer.parseInt(s.substring(1)) - 1;

        return new Coordinate(x,y);
    }
}
