
package battleship;
public class Cell {
    private State state;
    private Ship nave;

    
    public void setNave(Ship n) {
        nave = n;
    }
    public Cell(Ship N){
        nave = N;
        state = State.unchoose;
    }

    public Cell() {
        state = State.unchoose;
        nave = null;
    }

    public boolean naveAlive() {
        if(nave == null) return false;
        else return nave.isAlive();
    }

    public State getAttacked(){
        if(nave != null) {
            boolean affondata = nave.Hit();
            if (affondata) {
                state = State.dead;
            } else {
                state = State.hit;
            }
        }

        else{
            state = State.miss;
        }

        return state;
    }

    public State stato(){
        if (state == State.hit) {
            if (!naveAlive()) state = State.dead;
        }
        return state;
    }

    public boolean hasShip() {
        return nave != null;
    }
}
