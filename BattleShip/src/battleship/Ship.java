
package battleship;

public class Ship {

    private int mLife;

    private int getLife() {
        return mLife;
    }
    private void setLife(int mLife) {
        this.mLife = mLife;
    }

    public Ship(int length) {
        setLife(length);
    }

    public boolean isAlive()
    {
        return getLife() > 0;
    }

    public boolean Hit() {
        mLife--;
        return !isAlive();
    }
}

