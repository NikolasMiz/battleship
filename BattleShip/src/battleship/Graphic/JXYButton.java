/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship.Graphic;

import battleship.Coordinate;
import javax.swing.JButton;

/**
 *
 * @author Barbara
 */
public class JXYButton extends JButton{
    Coordinate xy;
    
    public JXYButton(int x, int y) {
        super();
        xy = new Coordinate(x,y);
    }
    
    public JXYButton(String a, int x, int y) {
        super(a);
        xy = new Coordinate(x,y);

    }
}
