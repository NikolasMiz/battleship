
package battleship.Graphic;  
import battleship.Game;
import battleship.Player;
import battleship.Player;
public interface GUI {     
    void render(Player p1, Player p2);
    void setP1(Player p);
    void setP2(Player p);
    void alert(String a);
    void disableP1();
    void disableP2();
    void setGame(Game game);
} 