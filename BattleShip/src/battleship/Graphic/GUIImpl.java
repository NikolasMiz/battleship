package battleship.Graphic;

import battleship.Game;
import battleship.Player;
import battleship.State;
import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GUIImpl implements GUI {

    JFrame court;
    JPanel pcourt;
    JXYButton[][] mcourt;

    JFrame opponent;
    JPanel popponent;
    JXYButton[][] mopponent;

    Player p1;
    Player p2;

    Game g;

    @Override
    public void setP1(Player p) {
        p1 = p;
    }
   @Override
    public void setP2(Player p) {
        p2 = p;
    }

    @Override
    public void alert(String a) {
        JOptionPane.showMessageDialog(court, a);
    }

    public GUIImpl() {


        mcourt = new JXYButton [10][10];
        mopponent = new JXYButton [10][10];


        court = new JFrame("Il tuo campo");
        court.setSize(400, 400);
        court.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        court.setVisible(true);
        pcourt = new JPanel(new GridLayout(11,11));
        pcourt.add(new JLabel(" "));
        pcourt.add(new JLabel("A"));
        pcourt.add(new JLabel("B"));
        pcourt.add(new JLabel("C"));
        pcourt.add(new JLabel("D"));
        pcourt.add(new JLabel("E"));
        pcourt.add(new JLabel("F"));
        pcourt.add(new JLabel("G"));
        pcourt.add(new JLabel("H"));
        pcourt.add(new JLabel("I"));
        pcourt.add(new JLabel("L"));
                         JOptionPane.showMessageDialog(court, "Inserisci una nave lunga 2 orizzontale");

        for(int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                if(x == 0) {
                    pcourt.add(new JLabel(String.valueOf(y + 1)));
                }
                JXYButton btt = new JXYButton(x,y);


                btt.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JXYButton xybtt = (JXYButton)e.getSource();
                        switch (p1.getShipNumber()) {
                            case 0:
                                if(!p1.Place(2, false, xybtt.xy)) {JOptionPane.showMessageDialog(court, "Errore"); return;}
                                p1.IncrementShip();
                                render(p1, p2);
                                JOptionPane.showMessageDialog(court, "Inserisci una nave lunga 2 verticale");
                                break;
                            case 1:
                                if(!p1.Place(2, true, xybtt.xy)) {JOptionPane.showMessageDialog(court, "Errore"); return;}
                                p1.IncrementShip();
                                                                render(p1, p2);

                                JOptionPane.showMessageDialog(court, "Inserisci una nave lunga 3 orizzontale");

                                break;
                            case 2:
                                if(!p1.Place(3, false, xybtt.xy)) {JOptionPane.showMessageDialog(court, "Errore"); return;}
                                render(p1, p2);
                                JOptionPane.showMessageDialog(court, "Inserisci una nave lunga 4 verticale");

                                p1.IncrementShip();

                                break;
                            case 3:
                                if(!p1.Place(4, true, xybtt.xy)) {JOptionPane.showMessageDialog(court, "Errore"); return;}
                                p1.IncrementShip();
                                render(p1, p2);
                                alert("Inizio gioco");
                                disableP1();
                                enableP2();
                                break;


                        }
                    }
                });


                mcourt[x][y] = btt;
                btt.setBackground(Color.blue);
                pcourt.add(btt);
            }
        }
        court.add(pcourt);
        court.pack();
        court.repaint();

        opponent = new JFrame("Avversario");
        opponent.setSize(400, 400);
        opponent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        opponent.setVisible(true);
        popponent = new JPanel(new GridLayout(11,11));
        popponent.add(new JLabel(" "));
        popponent.add(new JLabel("A"));
        popponent.add(new JLabel("B"));
        popponent.add(new JLabel("C"));
        popponent.add(new JLabel("D"));
        popponent.add(new JLabel("E"));
        popponent.add(new JLabel("F"));
        popponent.add(new JLabel("G"));
        popponent.add(new JLabel("H"));
        popponent.add(new JLabel("I"));
        popponent.add(new JLabel("L"));

        for(int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                if(x == 0) {
                    popponent.add(new JLabel(String.valueOf(y + 1)));
                }
                JXYButton btt = new JXYButton(x,y);
                btt.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JXYButton xybtt = (JXYButton)e.getSource();
                        State res = p2.getAttacked(xybtt.getX()/34 - 1, xybtt.getY()/16 -1);
                        switch (res) {
                            case hit:
                                alert("Hai colpito");
                                break;
                            case dead:
                                alert("Hai affondato una nave");
                                break;
                            case miss:
                                alert("Hai mancato");
                                break;
                            case repetition:
                                alert("Ripetizione");
                        }
                        if(g.win(p1)) {
                            alert("Hai vinto");
                            disableP1();
                            disableP2();
                            return;
                        }
                        g.forceRender();
                        g.p2RandomMove();
                    }
                });

                mopponent[x][y] = btt;
                btt.setEnabled(false);

                btt.setBackground(Color.blue);

                popponent.add(btt);
            }
        }
        opponent.add(popponent);

    }

    @Override
    public void render(Player p1, Player p2) {


        for(int y = 0; y < 10; y++) {
            for(int x = 0; x < 10; x++) {
                switch (p1.getCourt()[x][y].stato()) {
                    case unchoose:
                        if(p1.getCourt()[x][y].naveAlive())
                            mcourt[x][y].setBackground(Color.LIGHT_GRAY);
                        break;
                    case hit:
                        mcourt[x][y].setBackground(Color.orange);
                        break;
                    case dead:
                        mcourt[x][y].setBackground(Color.red);
                        break;
                }
            }
        }

        for(int y = 0; y < 10; y++) {
            for(int x = 0; x < 10; x++) {
                switch (p2.getCourt()[x][y].stato()) {
                    case miss:
                        mopponent[x][y].setBackground(Color.WHITE);
                        mopponent[x][y].setEnabled(false);
                        break;
                    case hit:
                        mopponent[x][y].setBackground(Color.orange);
                        mopponent[x][y].setEnabled(false);

                        break;
                    case dead:
                        mopponent[x][y].setBackground(Color.red);
                        mopponent[x][y].setEnabled(false);

                        break;
                }
            }
        }


        opponent.pack();
        opponent.repaint();
    }

    private void modifyEnable(JXYButton[][] s, boolean enable){
        for(int y = 0; y < 10; y++){
            for(int x = 0; x < 10; x++){
                s[x][y].setEnabled(enable);
            }
        }
    }

    @Override
    public void disableP1(){
        modifyEnable(mcourt, false);
    }

    @Override
        public void disableP2(){
        modifyEnable(mopponent, false);
    }

    @Override
    public void setGame(Game game) {
        g = game;
    }

    public void enableP1(){
        modifyEnable(mcourt, true);
    }

    public void enableP2(){
        modifyEnable(mopponent, true);
    }


}
