
package battleship;

public class Player {

    private Cell mCourt[][];
    int shipNumber;

    public int getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(int shipNumber) {
        this.shipNumber = shipNumber;
    }
    
    
    public Cell[][] getCourt() {
        return mCourt;
    }
    
    public void IncrementShip() {
        shipNumber++;
    }
    public Player(){
        shipNumber = 0;
        mCourt = new Cell[10][10];
        for (int x = 0; x < 10; x++){
            for(int y = 0; y<10; y++){
                mCourt[x][y] = new Cell();
            }
        }
    }

    public State getAttacked(int x, int y){
        if(mCourt[x][y].stato() != State.unchoose)
            return State.repetition;
        else{
            return mCourt[x][y].getAttacked();
        }
    }

    public boolean isAlive() {
        int life = 0;

        for (int x = 0; x < 10; x++){
            for(int y = 0; y<10; y++){
                if(mCourt[x][y].naveAlive())
                    life++;
            }
        }

        return life > 0;
    }
    
    public boolean Place(int length, boolean vertical, Coordinate xy){
        int mX = xy.getX();
        int mY = xy.getY();
        
        for(int i=0; i < length; i++) {
            if(mY >= 10 || mX >= 10) return false;
            if(getCourt()[mX][mY].naveAlive()) return false;
            
            else {
                if(vertical) mY++;
                else mX++;
            }
        }
        
        Ship s = new Ship(length);
        mX = xy.getX();
        mY = xy.getY();
        
        for(int i=0; i < length; i++) {
            getCourt()[mX][mY].setNave(s);
            if(vertical) mY++;
            else mX++;
        }
        return true;
    }
}

