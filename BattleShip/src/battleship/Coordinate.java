
package battleship;

public class Coordinate {
    private int mX;

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    private int mY;


    public Coordinate(int x, int y) {
        mX = x;
        mY = y;
    }
}

