
package battleship;

import battleship.Graphic.GUI;
import battleship.Graphic.GUIImpl;

import java.util.Random;

public class Game {
    private Player mPlayer1, mPlayer2;
    private GUI gui;

    public Game(GUI Gui){
        
        mPlayer1 = new Player();
        mPlayer2 = new Player();
        
        
        Gui.setP1(mPlayer1);
        Gui.setP2(mPlayer2);
        gui = Gui;
        
        gui.render(mPlayer1, mPlayer2);
        gui.setGame(this);

    }

    private State turno(Player a, Player b, int x, int y){
        State s = b.getAttacked(x,y);
        gui.render(mPlayer1, mPlayer2);
        return s;
    }

    public State turnoP1(int x, int y){
        return turno(mPlayer1, mPlayer2, x, y);
    }

    public State turnoP2(int x, int y){
        return turno(mPlayer2, mPlayer1, x, y);
    }

    public boolean win(Player p) {
        Player avversario;
        if(p == mPlayer1) avversario = mPlayer2;
        else
            avversario = mPlayer1;

        return !avversario.isAlive();
    }

    public boolean someoneWin() {
        return win(mPlayer1) || win(mPlayer2);
    }

    public void forceRender() {
        gui.render(mPlayer1, mPlayer2);
    }
    public void p2PlaceRandom() {
        Random r = new Random();

        while (!mPlayer2.Place(2, false, new Coordinate(r.nextInt(10), r.nextInt(10)))) {}
        while (!mPlayer2.Place(2, true, new Coordinate(r.nextInt(10), r.nextInt(10)))) {}
        while (!mPlayer2.Place(3, false, new Coordinate(r.nextInt(10), r.nextInt(10)))) {}
        while (!mPlayer2.Place(4, true, new Coordinate(r.nextInt(10), r.nextInt(10)))) {}
    }
    public void p2RandomMove() {
        Random r = new Random();

        int x = r.nextInt(10);
        int y = r.nextInt(10);
        State res = mPlayer1.getAttacked(x, y);
        while (res == State.repetition) {

             x = r.nextInt(10);
             y = r.nextInt(10);
             res = mPlayer1.getAttacked(x, y);
        }

        switch (res) {
            case hit:
                gui.alert("AI ti ha colpito");
                break;
            case dead:
                gui.alert("AI ti ha affondato una nave");
                break;
            case miss:
                gui.alert("AI ti ha mancato");
                break;
        }
        forceRender();

        if(win(mPlayer2)) { gui.alert("AI ha vinto");
        gui.disableP1();
        gui.disableP2();
        }
    }
   
}
